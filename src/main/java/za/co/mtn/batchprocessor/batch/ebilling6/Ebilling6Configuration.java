package za.co.mtn.batchprocessor.batch.ebilling6;

import org.springframework.core.io.FileSystemResource;
import za.co.mtn.batchprocessor.model.Ebilling6Record;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.LineMapper;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import za.co.mtn.batchprocessor.util.CustomStringUtil;

@Configuration
@EnableBatchProcessing
public class Ebilling6Configuration {

    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    @Value("${ebilling6.input.file.path}")
    private String inputFilePath;

    @Bean("Ebilling6Job")
    public Job ebilling6PreLoaderJob(@Qualifier("Ebilling6ValidateRecordStep") Step step){
        return jobBuilderFactory.get("Job:ebilling6-validator")
                .incrementer(new RunIdIncrementer())
                .flow(step)
                .end()
                .build();
    }

    @Bean("Ebilling6ValidateRecordStep")
    public Step ebilling6ValidateRecordsStep(){
        return stepBuilderFactory.get("Step:validate-ebilling6-records")
                .listener(new Ebilling6Listener())
                .chunk(20)
                .reader(ebilling6Reader())
                .processor(ebilling6Processor())
                .writer(ebilling6Writer())
                .build();
    }

    @Bean("Ebilling6Processor")
    public ItemProcessor ebilling6Processor() { return new Ebilling6Processor(); }

    @Bean("Ebilling6Writer")
    public ItemWriter<Ebilling6Record> ebilling6Writer(){ return new Ebilling6Writer(); }

    @Bean("Ebilling6Reader")
    public FlatFileItemReader<Ebilling6Record> ebilling6Reader() {
        return new FlatFileItemReaderBuilder<Ebilling6Record>()
                .name("Reader:ebilling6-dat-file-reader")
                .resource(new FileSystemResource(inputFilePath))
                .lineMapper(ebilling6lineMapper())
                .build();
    }

    @Bean("Ebilling6LineMapper")
    public LineMapper<Ebilling6Record> ebilling6lineMapper(){
        DefaultLineMapper<Ebilling6Record> defaultLineMapper = new DefaultLineMapper<>();
        DelimitedLineTokenizer lineTokenizer = new DelimitedLineTokenizer();

        lineTokenizer.setDelimiter("|");
        lineTokenizer.setNames(CustomStringUtil.getAllPublicFieldNames(new Ebilling6Record()));
        lineTokenizer.setStrict(false);

        BeanWrapperFieldSetMapper<Ebilling6Record> fieldSetMapper = new BeanWrapperFieldSetMapper<>();
        fieldSetMapper.setTargetType(Ebilling6Record.class);

        defaultLineMapper.setLineTokenizer(lineTokenizer);
        defaultLineMapper.setFieldSetMapper(fieldSetMapper);

        return defaultLineMapper;
    }

}
