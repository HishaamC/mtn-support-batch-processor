package za.co.mtn.batchprocessor.batch.ebilling6;

import za.co.mtn.batchprocessor.model.Ebilling6Record;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemWriter;

import java.util.List;

@Slf4j
public class Ebilling6Writer implements ItemWriter<Ebilling6Record> {

    @Override
    public void write(List<? extends Ebilling6Record> list) throws Exception {
    }
}
