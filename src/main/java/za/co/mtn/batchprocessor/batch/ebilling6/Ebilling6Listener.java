package za.co.mtn.batchprocessor.batch.ebilling6;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;

public class Ebilling6Listener implements StepExecutionListener {

    @Override
    public void beforeStep(StepExecution stepExecution) {
        System.out.println("Starting...");
    }

    @Override
    public ExitStatus afterStep(StepExecution stepExecution) {
        System.out.println("Finished..." );
        System.out.println("Amount records read : " + stepExecution.getReadCount());
        System.out.println("Amount records skipped : " + stepExecution.getSkipCount());
        return null;
    }
}
