package za.co.mtn.batchprocessor.batch.vas;

import za.co.mtn.batchprocessor.model.ValueAddedService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.batch.item.ItemProcessor;

@Slf4j
public class ValueAddedServiceProcessor implements ItemProcessor<ValueAddedService,ValueAddedService> {

    @Autowired
    @Qualifier(value = "valueAddedServiceChecksum")
    ValueAddedServiceChecksum valueAddedServiceChecksum;
    @Override
    public ValueAddedService process(ValueAddedService vas) throws Exception {
        ValueAddedService newVas = new ValueAddedService();
        if(!vas.getServiceCode().equalsIgnoreCase("") ) newVas.setServiceCode(vas.getServiceCode());
        if(!vas.getServiceFlag().equalsIgnoreCase("") ) newVas.setServiceFlag(vas.getServiceFlag());
        if(!vas.getServiceName().equalsIgnoreCase("") ) newVas.setServiceName(vas.getServiceName());
        if(!vas.getServiceDescription().equalsIgnoreCase("") ) newVas.setServiceDescription(vas.getServiceDescription());
        if(!vas.getActivateInternalTandcId().equalsIgnoreCase("") ) newVas.setActivateInternalTandcId(vas.getActivateInternalTandcId());
        if(!vas.getDeactivateInternalTandcId().equalsIgnoreCase("") ) newVas.setDeactivateInternalTandcId(vas.getDeactivateInternalTandcId());
        if(valueAddedServiceChecksum.isVasValid(newVas)){
            return newVas;
        }
        return null;
    }
}
