package za.co.mtn.batchprocessor.batch.vas;

import za.co.mtn.batchprocessor.model.ValueAddedService;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.LineMapper;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.builder.FlatFileItemWriterBuilder;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.BeanWrapperFieldExtractor;
import org.springframework.batch.item.file.transform.DelimitedLineAggregator;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import za.co.mtn.batchprocessor.util.CustomStringUtil;

@Configuration
@EnableBatchProcessing
public class ValueAddedServiceConfiguration {

    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    @Value("${vas.input.file.path}")
    private String inputFilePath;

    @Value("${vas.output.file.path}")
    private String outputFilePath;

    @Bean("VasJob")
    public Job VasJob(@Qualifier("VasCreateScriptStep") Step step){
        return jobBuilderFactory.get("Job:value-added-services")
                .incrementer(new RunIdIncrementer())
                .flow(step)
                .end()
                .build();
    }

    @Bean("VasCreateScriptStep")
    public Step generateVasScriptStep(){
        return stepBuilderFactory.get("Step:generate-vas-script")
                .chunk(200)
                .reader(vasReader())
                .processor(vasProcessor())
                .writer(vasWriter())
                .build();
    }

    @Bean("VasProcessor")
    public ItemProcessor vasProcessor() { return new ValueAddedServiceProcessor(); }

    @Bean("VasReader")
    public FlatFileItemReader<ValueAddedService> vasReader() {
        return new FlatFileItemReaderBuilder<ValueAddedService>()
                .name("Reader:vas-csv-reader")
                .resource(new FileSystemResource(inputFilePath))
                .lineMapper(vasLineMapper())
                .build();
    }

    @Bean("VasLineMapper")
    public LineMapper<ValueAddedService> vasLineMapper(){
        DefaultLineMapper<ValueAddedService> defaultLineMapper = new DefaultLineMapper<>();
        DelimitedLineTokenizer lineTokenizer = new DelimitedLineTokenizer();

        lineTokenizer.setDelimiter(",");
        lineTokenizer.setNames(CustomStringUtil.getAllPublicFieldNames(new ValueAddedService()));
        lineTokenizer.setStrict(false);

        BeanWrapperFieldSetMapper<ValueAddedService> fieldSetMapper = new BeanWrapperFieldSetMapper<>();
        fieldSetMapper.setTargetType(ValueAddedService.class);

        defaultLineMapper.setLineTokenizer(lineTokenizer);
        defaultLineMapper.setFieldSetMapper(fieldSetMapper);

        return defaultLineMapper;
    }

    @Bean
    public FlatFileItemWriter<ValueAddedService> vasWriter(){
        Resource resource = new FileSystemResource(outputFilePath);
        return new FlatFileItemWriterBuilder<ValueAddedService>()
                .name("VasFileWriter")
                .resource(resource)
                .shouldDeleteIfExists(true)
                .lineAggregator(new DelimitedLineAggregator<ValueAddedService>() {
                    {
                        setDelimiter(",");
                        setFieldExtractor(new BeanWrapperFieldExtractor<ValueAddedService>() {
                            {
                                setNames(new String[] {"statement"});
                            }
                        });
                    }
                })
                .build();
    }
}
