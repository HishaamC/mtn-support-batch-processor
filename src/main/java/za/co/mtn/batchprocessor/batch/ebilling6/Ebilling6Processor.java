package za.co.mtn.batchprocessor.batch.ebilling6;

import za.co.mtn.batchprocessor.model.Ebilling6Record;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;

public class Ebilling6Processor implements ItemProcessor<Ebilling6Record, Ebilling6Record> {

    @Autowired
    Ebilling6CheckSum checkSum;

    @Override
    public Ebilling6Record process(Ebilling6Record record) throws Exception {
        checkSum.hasValidRecordType(record);
        return record;
    }
}
