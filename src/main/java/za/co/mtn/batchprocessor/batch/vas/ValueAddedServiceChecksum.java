package za.co.mtn.batchprocessor.batch.vas;

import za.co.mtn.batchprocessor.model.ValueAddedService;
import za.co.mtn.batchprocessor.model.VasTermsAndConditions;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class ValueAddedServiceChecksum {

    public boolean isVasValid(ValueAddedService vas){
        int activeTandCId;
        int deactiveTandCId;

        // is field correct type
        try {
            activeTandCId = Integer.valueOf(vas.getActivateInternalTandcId());
            deactiveTandCId = Integer.valueOf(vas.getDeactivateInternalTandcId());
        }catch (NumberFormatException nfe){
            log.error("Validation Error : InternalTandcId not a number : {}",vas.toString());
            return false;
        }

        // does T&C id exits
        if(!VasTermsAndConditions.isVasTandC(activeTandCId) && !VasTermsAndConditions.isVasTandC(deactiveTandCId)){
            log.error("Validation Error : Terms and condition code does not exist : {}",vas.toString());
            return false;
        }

        // check required fields are null
        try {
            if(vas.getServiceCode() == null || vas.getServiceFlag() == null) throw new NullPointerException();
        }catch (NullPointerException npe){
            log.error("Validation Error : Field {} & {} cannot be null : {}","serviceCode","serviceFlag",vas.toString());
            return false;
        }

        return true;
    }
}
