package za.co.mtn.batchprocessor.batch.ebilling6;

import za.co.mtn.batchprocessor.model.Ebilling6Record;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import za.co.mtn.batchprocessor.model.Ebilling6RecordTypes;

@Slf4j
@Component
public class Ebilling6CheckSum {

    public void hasValidRecordType(Ebilling6Record record){
        try {
            int recordType = Integer.valueOf(record.getRecordType());
            if(!Ebilling6RecordTypes.isValidRecordType(recordType)){
                throw new NumberFormatException();
            }
        }catch (NumberFormatException nfe){
            log.error("Not a valid record type : {} ________ {}",record.getRecordType(),record);
        }
    }
}
