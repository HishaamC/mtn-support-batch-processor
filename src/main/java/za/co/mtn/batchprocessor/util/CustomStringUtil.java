package za.co.mtn.batchprocessor.util;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringEscapeUtils;
import org.springframework.data.jpa.repository.query.EscapeCharacter;

import java.util.ArrayList;
import java.util.List;

@Slf4j
public class CustomStringUtil {


    /*
    * Return list of all field names in a class that are declared public
    */
    public static String[] getAllPublicFieldNames(Object fieldObject) {
        java.lang.reflect.Field[] allFields = fieldObject.getClass().getFields();
        String[] fieldNamesArray = new String[allFields.length];
        for (int i = 0; i < fieldNamesArray.length ; i++) {
            fieldNamesArray[i] = allFields[i].getName();
        }
        return fieldNamesArray;
    }

    public static String escapeSingleQuotes(String str){
        str = str.replaceAll("\\'","\\\\'");
        return str;
    }

}
