package za.co.mtn.batchprocessor.model;

import lombok.Data;

/*
* Ebilling6Record : Used to map input from b2c.dat, b2b.dat and master.dat files
*/
@Data
public class Ebilling6Record {

    public String recordType;
    public String fieldPosition_1;
    public String fieldPosition_2;
    public String fieldPosition_3;
    public String fieldPosition_4;
    public String fieldPosition_5;
    public String fieldPosition_6;
    public String fieldPosition_7;
    public String fieldPosition_8;
    public String fieldPosition_9;
    public String fieldPosition_10;
    public String fieldPosition_11;
    public String fieldPosition_12;
    public String fieldPosition_13;
    public String fieldPosition_14;
    public String fieldPosition_15;
    public String fieldPosition_16;
    public String fieldPosition_17;
    public String fieldPosition_18;
    public String fieldPosition_19;
    public String fieldPosition_20;
    public String fieldPosition_21;
    public String fieldPosition_22;
    public String fieldPosition_23;
    public String fieldPosition_24;
    public String fieldPosition_25;
    public String fieldPosition_26;
    public String fieldPosition_27;
    public String fieldPosition_28;
    public String fieldPosition_29;
    public String fieldPosition_30;
    public String fieldPosition_31;
    public String fieldPosition_32;
    public String fieldPosition_33;
    public String fieldPosition_34;
    public String fieldPosition_35;
    public String fieldPosition_36;
    public String fieldPosition_37;
    public String fieldPosition_38;
    public String fieldPosition_39;
    public String fieldPosition_40;
    public String fieldPosition_41;
    public String fieldPosition_42;

    @Override
    public String toString() {
        return "Record{[ " + recordType +
                "|" + fieldPosition_1  +
                "|" + fieldPosition_2  +
                "|" + fieldPosition_3  +
                "|" + fieldPosition_4  +
                "|" + fieldPosition_5  +
                "|" + fieldPosition_6  +
                "|" + fieldPosition_7  +
                "|" + fieldPosition_8  +
                "|" + fieldPosition_9  +
                "|" + fieldPosition_10 +
                "|" + fieldPosition_11 +
                "|" + fieldPosition_12 +
                "|" + fieldPosition_13 +
                "|" + fieldPosition_14 +
                "|" + fieldPosition_15 +
                "|" + fieldPosition_16 +
                "|" + fieldPosition_17 +
                "|" + fieldPosition_18 +
                "|" + fieldPosition_19 +
                "|" + fieldPosition_20 +
                "|" + fieldPosition_21 +
                "|" + fieldPosition_22 +
                "|" + fieldPosition_23 +
                "|" + fieldPosition_24 +
                "|" + fieldPosition_25 +
                "|" + fieldPosition_26 +
                "|" + fieldPosition_27 +
                "|" + fieldPosition_28 +
                "|" + fieldPosition_29 +
                "|" + fieldPosition_30 +
                "|" + fieldPosition_31 +
                "|" + fieldPosition_32 +
                "|" + fieldPosition_33 +
                "|" + fieldPosition_34 +
                "|" + fieldPosition_35 +
                "|" + fieldPosition_36 +
                "|" + fieldPosition_37 +
                "|" + fieldPosition_38 +
                "|" + fieldPosition_39 +
                "|" + fieldPosition_40 +
                "|" + fieldPosition_41 +
                "|" + fieldPosition_42 +
                " ]}";
    }
}
