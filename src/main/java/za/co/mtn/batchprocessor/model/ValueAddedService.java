package za.co.mtn.batchprocessor.model;

import lombok.Data;
import za.co.mtn.batchprocessor.util.CustomStringUtil;

@Data
public class ValueAddedService {

    public String serviceCode;

    public String serviceFlag;

    public String serviceName;

    public String serviceDescription;

    public String activateInternalTandcId;

    public String deactivateInternalTandcId;

    private String statement;

    @Override
    public String toString() {
        return "ValueAddedService{" +
                "serviceCode='" + serviceCode + '\'' +
                ", serviceFlag='" + serviceFlag + '\'' +
                ", serviceName='" + serviceName + '\'' +
                ", serviceDescription='" + serviceDescription + '\'' +
                ", activateInternalTandcId='" + activateInternalTandcId + '\'' +
                ", deactivateInternalTandcId='" + deactivateInternalTandcId + '\'' +
                '}';
    }

    public String getStatement(){
        StringBuilder statement = new StringBuilder();
        statement.append("INSERT INTO VALUE_ADDED_SERVICE (SERVICE_CODE,SERVICE_FLAG,SERVICE_NAME,SERVICE_DESCRIPTION,ACTIVATE_INTERNAL_TANDC_ID,DEACTIVATE_INTERNAL_TANDC_ID) VALUES (");
        statement.append("'").append(CustomStringUtil.escapeSingleQuotes(this.serviceCode)).append("',");
        statement.append("'").append(CustomStringUtil.escapeSingleQuotes(this.serviceFlag)).append("',");
        statement.append("'").append(CustomStringUtil.escapeSingleQuotes(this.serviceName)).append("',");
        statement.append("'").append(CustomStringUtil.escapeSingleQuotes(this.serviceDescription)).append("',");
        statement.append(this.activateInternalTandcId).append(",");
        statement.append(this.deactivateInternalTandcId).append(")").append(";"); //end statement
        this.statement = statement.toString().replaceAll("'null'","null");
        return this.statement;
    }
}
