package za.co.mtn.batchprocessor.model;

import lombok.Getter;

public enum Ebilling6RecordTypes {

    DOCUMENT_TYPE                                (0000),

    //MASTER DATA RECORD TYPES
    PAYMENT_DIMENSION_                           (100),
    ADJUSTMENT_DIMENSION                         (110),
    CHARGE_DIMENSION                             (120),
    SUB_CHARGE_DIMENSION                         (130),
    PLAN_DIMENSION                               (140),
    PRODUCT_AND_SUB_PRODUCT_DIMENSION            (150),
    SERVICE_USAGE_DIMENSION                      (160),
    TARIFF_DIMENSION                             (170),
    REGION_DIMENSION                             (180),
    CARRIER_DIMENSION                            (190),
    AREA_DIMENSION                               (200),

    //B2B & B2C RECORD TYPES
    SUMMARY_LEVEL_DETAIL_RECORD                  (1000),
    STATEMENT_PAYMENT_FACT                       (1100),
    STATEMENT_ADJUSTMENT_FACT                    (1200),
    CHARGE_SUMMARY_FOR_ACCOUNT                   (2000),
    ACCOUNT_LEVEL_CHARGES_AT_CHARGE_TYPE_LEVEL   (2100),
    SERVICE_LEVEL_TOTAL                          (3000),
    SERVICE_LEVEL_ON_CHARGE_TYPE                 (3100),
    SERVICE_CHARGE_ON_PRODUCT_AND_PLAN           (3200),
    SERVICE_CHARGE_ON_USAGE_TYPE                 (3300),
    SERVICE_CHARGES_ON_VARIOUS_TARIFF            (3400),
    SERVICE_CALL_USAGE_DETAILS                   (4000);

    @Getter
    private final int recordTypeCode;

    Ebilling6RecordTypes(int recordTypeCode) {
       this.recordTypeCode=recordTypeCode;
    }

    public static boolean isValidRecordType(int recordCode){
        for (Ebilling6RecordTypes recordTypes : Ebilling6RecordTypes.values()) {
            if(recordCode == recordTypes.getRecordTypeCode()){
                return true;
            }
        }
        return false;
    }
}
