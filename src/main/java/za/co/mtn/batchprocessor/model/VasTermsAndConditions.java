package za.co.mtn.batchprocessor.model;

public enum VasTermsAndConditions {
    TANDC_ACTIVATE                      (1 ,"Activate"),
    TANDC_DE_ACTIVATE                   (2 ,"De-Activate"),
    TANDC_SMARTPHONEBUNDLESUNCAPPED     (3 ,"SmartPhoneBundlesUncapped"),
    TANDC_IR                            (4 ,"IR"),
    TANDC_BLACKBERRY                    (5 ,"BlackBerry"),
    TANDC_ANYTIMEMINUTEADDON            (6 ,"AnyTimeMinuteAddOn"),
    TANDC_MTNTOMTNMINUTEADDON           (7 ,"MTNtoMTNMinuteAddOn"),
    TANDC_SMSADDON                      (8 ,"SMSAddOn"),
    TANDC_NOTIFICATIONS                 (9 ,"Notifications"),
    TANDC_MTNCHOICEMINUTE               (10,"MTNChoiceMinute"),
    TANDC_MTNCHOICEVALUE                (11,"MTNChoiceValue"),
    TANDC_QHUBEKA                       (12,"Qhubeka"),
    TANDC_SMARTPHONEBUNDLESCHAT         (13,"SmartPhoneBundlesChat"),
    TANDC_SMARTPHONEBUNDLESSOCIALNET    (14,"SmartPhoneBundlesSocialNet"),
    TANDC_SMARTPHONEBUNDLESSPLITE       (15,"SmartPhoneBundlesSPLite"),
    TANDC_SMARTPHONEBUNDLESSPPLUS       (16,"SmartPhoneBundlesSPPlus"),
    TANDC_SMARTPHONEBUNDLESIE           (17,"SmartPhoneBundlesIE"),
    TANDC_INTERNET_BUNDLES              (18,"Internet Bundles"),
    TANDC_SMS_BUNDLES                   (19,"SMS Bundles"),
    TANDC_INTERNATIONAL_WIFI_ROAMING    (20,"International Wifi Roaming"),
    TANDC_INTERNATIONAL_CALLING_BUNDLES (21,"International Calling Bundles"),
    TANDC_ME2U                          (25,"Me2U");


    private int code;
    private String desc;

    VasTermsAndConditions(int code, String desc) {
        this.code=code;
        this.desc=desc;
    }

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

    public static boolean isVasTandC(Integer code){
        for (VasTermsAndConditions termsAndCondition : VasTermsAndConditions.values()) {
            if(code == termsAndCondition.getCode()) return true;
        }
        return false;
    }
}
