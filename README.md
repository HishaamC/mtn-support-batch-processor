# Prerequisites #
1. [Setup Java 8 on local machine](https://eohdigital.atlassian.net/wiki/spaces/MTN/pages/597776/Environment+Setup#EnvironmentSetup-InstallationofJava)
2. [Setup maven 3 on local machine](https://eohdigital.atlassian.net/wiki/spaces/MTN/pages/597776/Environment+Setup#EnvironmentSetup-InstallationandConfigurationofMaven)

# Build application #
1. In root directory run command : mvn package
2. Copy zip file from target folder
3. Extract zip file in location of choice on your machine
######
    Folder Structure:
    * cfg
        * application.properties
    * input
        * ebilling6
        * vas
    * ouput
        * vas
    * lib
    * .jar (application jar)
    * .sh (job scripts)
######
    
# Ebilling 6 Dat file validator job #
#### Responsible for validating the files generated from ebilling preprocessor ####
1. Copy .Dat file into folder : ./input/ebilling6
2. Update property "ebilling6.input.file.path" in ./cfg/application.properties with the file copied name
3. Run script ./Job-ebilling6-validator.sh

# VAS script generator job #
#### Responsible for converting .csv file into sql script for adding new vas services to OLTP.VALUE_ADDED_SERVICE ####
1. Create .csv file wih following line format : serviceCode,serviceFlag,serviceName,serviceDescription,activateInternalTandcId,deactivateInternalTandcId 
    1. serviceCode (String)
    2. serviceFlag (String)
    3. serviceName (String)
    4. serviceDescription (String)
    5. activateInternalTandcId (Integer)
    6. deactivateInternalTandcId (Integer)
2. Copy .csv file into folder : ./input/vas
3. Update property "vas.output.file.path" in ./cfg/application.properties with the file copied name
4. Run script ./Job-vas-script-generator.sh
5. Find output sql script in ./output/vas

# Run a specific job from command line#
1. Open terminal/cmd in project root directory : /mtn-support-batch-processor
2. Run command : mvn clean install
3. java -jar target/support-batch-processor-0.0.1-SNAPSHOT.jar --spring.batch.job.names=[replace with job name]()
4. See : [Job Names](#job-names)

# Job names #
- Job:ebilling6-validator
- Job:value-added-services